﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Configuration;


namespace Disiplin_Proje
{
    public partial class Form4 : Form
    {
        public Form4()
        {
            InitializeComponent();
        }
        SqlConnection Con = new SqlConnection(ConfigurationManager.ConnectionStrings["Baglanti"].ConnectionString);
        public  void Verileri_cek(string veri)
        {
            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(veri,Con);
            DataSet dataSet = new DataSet();
            sqlDataAdapter.Fill(dataSet);
            dataGridView1.DataSource = dataSet.Tables[0];
        }
      

        private void btnsorgula_Click(object sender, EventArgs e)
        {
            try
            {
                if (Con.State!=ConnectionState.Open)
                {
                    Con.Open();
                    string sorgu = "Select * from ogrenci where ogrenci_no ='" + txtogrnosorgula.Text + "'";
                    SqlCommand komut = new SqlCommand(sorgu, Con);
                    SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(komut);
                    DataSet dataSet = new DataSet();
                    sqlDataAdapter.Fill(dataSet);
                    dataGridView1.DataSource = dataSet.Tables[0];
                    SqlDataAdapter Adapter = new SqlDataAdapter("select * from Ogrenci where Sinifid='" + cmbogrsıifsorgula.SelectedValue + "'Order by No DESC", Con);
                    DataSet ds = new DataSet();
                    Adapter.Fill(ds);
                    dataGridView1.DataSource = ds.Tables[0];
                }
              
            }
            catch (Exception hata)
            {
                MessageBox.Show("Veriler gösterilemedi çünkü : ", hata.Message);
            }
            finally
            {
                Con.Close();
            }
        }


        private void öĞRENCİİŞLEMLERİToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form3 ogrenciislem = new Form3();
            this.Hide();
            ogrenciislem.Show();

        }

        private void dİSLİNSÖZLEŞMESİToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form5 _5_Sözlesme = new Form5();
            _5_Sözlesme.ShowDialog();
        }

        private void dİSİPLİNPROSEDÜRÜToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void Form4_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void Form4_Load(object sender, EventArgs e)
        {

            try
            {
                if (Con.State!=ConnectionState.Open)
                {
                    Con.Open();
                    Dictionary<int, string> Sinifcek = new Dictionary<int, string>();
                    string sorgu = "Select* from sinif";
                    SqlCommand komut = new SqlCommand(sorgu, Con);
                    SqlDataReader reader = komut.ExecuteReader();
                    while (reader.Read())
                    {
                        Sinifcek.Add(reader.GetInt32(0), reader.GetValue(1).ToString());
                    }
                    cmbogrsıifsorgula.DataSource = new BindingSource(Sinifcek, null);
                    cmbogrsıifsorgula.DisplayMember = "Value";
                    cmbogrsıifsorgula.ValueMember = "Key";
                }

                dataGridView1.Columns[0].Visible = true;
                dataGridView1.Columns[1].HeaderText = "Ad";
                dataGridView1.Columns[2].HeaderText = "Soyad";
                dataGridView1.Columns[3].HeaderText = "Öğrenci no";
                dataGridView1.Columns[4].HeaderText = "TC";
                dataGridView1.Columns[5].HeaderText = "Fotoğraf";
                dataGridView1.Columns[6].HeaderText = "Sınıf";
                dataGridView1.Columns[7].HeaderText = "Doğum tarihi";
                dataGridView1.Columns[8].HeaderText = "Cinsiyet";
                dataGridView1.Columns[9].Visible = true;
                dataGridView1.Columns[10].HeaderText = "Bölüm";

            }
            catch (Exception hata)
            {

                MessageBox.Show("Hata : ",hata.Message);
            }
            finally
            {
                Con.Close();
            }
        }
    }
}
