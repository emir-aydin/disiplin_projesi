﻿namespace Disiplin_Proje
{
    partial class Form3
    {
        /// <summary>
        ///Gerekli tasarımcı değişkeni.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///Kullanılan tüm kaynakları temizleyin.
        /// </summary>
        ///<param name="disposing">yönetilen kaynaklar dispose edilmeliyse doğru; aksi halde yanlış.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer üretilen kod

        /// <summary>
        /// Tasarımcı desteği için gerekli metot - bu metodun 
        ///içeriğini kod düzenleyici ile değiştirmeyin.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.öğrenciToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dİSİPLİNİŞLEMİToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dİSİPLİNİŞLEMLERİToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dİSİPLİNPROSEDÜRÜToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dİSİPİLİNSÖZLEŞMESİToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.şİKAYETKUTUSUToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aYARLARToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.btnsorgula = new System.Windows.Forms.Button();
            this.cmbsinif = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtograd = new System.Windows.Forms.TextBox();
            this.txtogrsoyad = new System.Windows.Forms.TextBox();
            this.txttc = new System.Windows.Forms.TextBox();
            this.txtogrno = new System.Windows.Forms.TextBox();
            this.txtogrveliad = new System.Windows.Forms.TextBox();
            this.txtogvelisoyad = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.cmbogrcinsiyet = new System.Windows.Forms.ComboBox();
            this.txtogveliad = new System.Windows.Forms.Label();
            this.txtogrvelisoyad = new System.Windows.Forms.Label();
            this.txtogvelinumara = new System.Windows.Forms.Label();
            this.txtogvelimail = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.mtxtogrvelinumara = new System.Windows.Forms.MaskedTextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnogrfoto = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.button4 = new System.Windows.Forms.Button();
            this.btnguncelle = new System.Windows.Forms.Button();
            this.btnkaydet = new System.Windows.Forms.Button();
            this.btnsil = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Dock = System.Windows.Forms.DockStyle.Left;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.öğrenciToolStripMenuItem,
            this.dİSİPLİNİŞLEMİToolStripMenuItem,
            this.şİKAYETKUTUSUToolStripMenuItem,
            this.aYARLARToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(126, 462);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // öğrenciToolStripMenuItem
            // 
            this.öğrenciToolStripMenuItem.Name = "öğrenciToolStripMenuItem";
            this.öğrenciToolStripMenuItem.Size = new System.Drawing.Size(113, 19);
            this.öğrenciToolStripMenuItem.Text = " ÖĞRENCİ İŞLEMİ";
            this.öğrenciToolStripMenuItem.Click += new System.EventHandler(this.ÖğrenciToolStripMenuItem_Click);
            // 
            // dİSİPLİNİŞLEMİToolStripMenuItem
            // 
            this.dİSİPLİNİŞLEMİToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dİSİPLİNİŞLEMLERİToolStripMenuItem,
            this.dİSİPLİNPROSEDÜRÜToolStripMenuItem,
            this.dİSİPİLİNSÖZLEŞMESİToolStripMenuItem});
            this.dİSİPLİNİŞLEMİToolStripMenuItem.Name = "dİSİPLİNİŞLEMİToolStripMenuItem";
            this.dİSİPLİNİŞLEMİToolStripMenuItem.Size = new System.Drawing.Size(113, 19);
            this.dİSİPLİNİŞLEMİToolStripMenuItem.Text = "DİSİPLİN İŞLEMİ";
            this.dİSİPLİNİŞLEMİToolStripMenuItem.Click += new System.EventHandler(this.dİSİPLİNİŞLEMİToolStripMenuItem_Click);
            // 
            // dİSİPLİNİŞLEMLERİToolStripMenuItem
            // 
            this.dİSİPLİNİŞLEMLERİToolStripMenuItem.Name = "dİSİPLİNİŞLEMLERİToolStripMenuItem";
            this.dİSİPLİNİŞLEMLERİToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.dİSİPLİNİŞLEMLERİToolStripMenuItem.Text = "DİSİPLİN İŞLEMLERİ";
            this.dİSİPLİNİŞLEMLERİToolStripMenuItem.Click += new System.EventHandler(this.dİSİPLİNİŞLEMLERİToolStripMenuItem_Click);
            // 
            // dİSİPLİNPROSEDÜRÜToolStripMenuItem
            // 
            this.dİSİPLİNPROSEDÜRÜToolStripMenuItem.Name = "dİSİPLİNPROSEDÜRÜToolStripMenuItem";
            this.dİSİPLİNPROSEDÜRÜToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.dİSİPLİNPROSEDÜRÜToolStripMenuItem.Text = "DİSİPLİN PROSEDÜRÜ";
            this.dİSİPLİNPROSEDÜRÜToolStripMenuItem.Click += new System.EventHandler(this.dİSİPLİNPROSEDÜRÜToolStripMenuItem_Click);
            // 
            // dİSİPİLİNSÖZLEŞMESİToolStripMenuItem
            // 
            this.dİSİPİLİNSÖZLEŞMESİToolStripMenuItem.Name = "dİSİPİLİNSÖZLEŞMESİToolStripMenuItem";
            this.dİSİPİLİNSÖZLEŞMESİToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.dİSİPİLİNSÖZLEŞMESİToolStripMenuItem.Text = "DİSİPİLİN SÖZLEŞMESİ";
            this.dİSİPİLİNSÖZLEŞMESİToolStripMenuItem.Click += new System.EventHandler(this.dİSİPİLİNSÖZLEŞMESİToolStripMenuItem_Click);
            // 
            // şİKAYETKUTUSUToolStripMenuItem
            // 
            this.şİKAYETKUTUSUToolStripMenuItem.Name = "şİKAYETKUTUSUToolStripMenuItem";
            this.şİKAYETKUTUSUToolStripMenuItem.Size = new System.Drawing.Size(113, 19);
            this.şİKAYETKUTUSUToolStripMenuItem.Text = "ŞİKAYET KUTUSU";
            // 
            // aYARLARToolStripMenuItem
            // 
            this.aYARLARToolStripMenuItem.Name = "aYARLARToolStripMenuItem";
            this.aYARLARToolStripMenuItem.Size = new System.Drawing.Size(113, 19);
            this.aYARLARToolStripMenuItem.Text = "AYARLAR";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dataGridView2);
            this.groupBox1.Controls.Add(this.dataGridView1);
            this.groupBox1.Controls.Add(this.btnsorgula);
            this.groupBox1.Controls.Add(this.cmbsinif);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(163, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(669, 206);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Ögrenci Sorgulama";
            // 
            // dataGridView2
            // 
            this.dataGridView2.BackgroundColor = System.Drawing.Color.PaleTurquoise;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Location = new System.Drawing.Point(375, 44);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.Size = new System.Drawing.Size(254, 150);
            this.dataGridView2.TabIndex = 4;
            // 
            // dataGridView1
            // 
            this.dataGridView1.BackgroundColor = System.Drawing.Color.PaleTurquoise;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(36, 44);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(229, 150);
            this.dataGridView1.TabIndex = 3;
            // 
            // btnsorgula
            // 
            this.btnsorgula.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.btnsorgula.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnsorgula.Location = new System.Drawing.Point(199, 19);
            this.btnsorgula.Name = "btnsorgula";
            this.btnsorgula.Size = new System.Drawing.Size(75, 21);
            this.btnsorgula.TabIndex = 2;
            this.btnsorgula.Text = "Sorgula";
            this.btnsorgula.UseVisualStyleBackColor = false;
            // 
            // cmbsinif
            // 
            this.cmbsinif.BackColor = System.Drawing.Color.PaleTurquoise;
            this.cmbsinif.FormattingEnabled = true;
            this.cmbsinif.Location = new System.Drawing.Point(72, 19);
            this.cmbsinif.Name = "cmbsinif";
            this.cmbsinif.Size = new System.Drawing.Size(121, 21);
            this.cmbsinif.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(33, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Sınıf :";
            // 
            // txtograd
            // 
            this.txtograd.BackColor = System.Drawing.Color.PaleTurquoise;
            this.txtograd.Location = new System.Drawing.Point(68, 22);
            this.txtograd.Name = "txtograd";
            this.txtograd.Size = new System.Drawing.Size(100, 20);
            this.txtograd.TabIndex = 2;
            // 
            // txtogrsoyad
            // 
            this.txtogrsoyad.BackColor = System.Drawing.Color.PaleTurquoise;
            this.txtogrsoyad.Location = new System.Drawing.Point(68, 48);
            this.txtogrsoyad.Name = "txtogrsoyad";
            this.txtogrsoyad.Size = new System.Drawing.Size(100, 20);
            this.txtogrsoyad.TabIndex = 3;
            // 
            // txttc
            // 
            this.txttc.BackColor = System.Drawing.Color.PaleTurquoise;
            this.txttc.Location = new System.Drawing.Point(68, 74);
            this.txttc.Name = "txttc";
            this.txttc.Size = new System.Drawing.Size(100, 20);
            this.txttc.TabIndex = 4;
            // 
            // txtogrno
            // 
            this.txtogrno.BackColor = System.Drawing.Color.PaleTurquoise;
            this.txtogrno.Location = new System.Drawing.Point(68, 100);
            this.txtogrno.Name = "txtogrno";
            this.txtogrno.Size = new System.Drawing.Size(100, 20);
            this.txtogrno.TabIndex = 5;
            // 
            // txtogrveliad
            // 
            this.txtogrveliad.BackColor = System.Drawing.Color.PaleTurquoise;
            this.txtogrveliad.Location = new System.Drawing.Point(90, 19);
            this.txtogrveliad.Name = "txtogrveliad";
            this.txtogrveliad.Size = new System.Drawing.Size(132, 20);
            this.txtogrveliad.TabIndex = 6;
            // 
            // txtogvelisoyad
            // 
            this.txtogvelisoyad.BackColor = System.Drawing.Color.PaleTurquoise;
            this.txtogvelisoyad.Location = new System.Drawing.Point(90, 45);
            this.txtogvelisoyad.Name = "txtogvelisoyad";
            this.txtogvelisoyad.Size = new System.Drawing.Size(132, 20);
            this.txtogvelisoyad.TabIndex = 7;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(19, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(26, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Ad :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(19, 55);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(43, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Soyad :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(19, 81);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(26, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Tc :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(19, 107);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(44, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Numara";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(19, 129);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(49, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "Cinsiyet :";
            // 
            // cmbogrcinsiyet
            // 
            this.cmbogrcinsiyet.BackColor = System.Drawing.Color.PaleTurquoise;
            this.cmbogrcinsiyet.FormattingEnabled = true;
            this.cmbogrcinsiyet.Location = new System.Drawing.Point(68, 126);
            this.cmbogrcinsiyet.Name = "cmbogrcinsiyet";
            this.cmbogrcinsiyet.Size = new System.Drawing.Size(100, 21);
            this.cmbogrcinsiyet.TabIndex = 13;
            // 
            // txtogveliad
            // 
            this.txtogveliad.AutoSize = true;
            this.txtogveliad.Location = new System.Drawing.Point(35, 22);
            this.txtogveliad.Name = "txtogveliad";
            this.txtogveliad.Size = new System.Drawing.Size(26, 13);
            this.txtogveliad.TabIndex = 14;
            this.txtogveliad.Text = "Ad :";
            // 
            // txtogrvelisoyad
            // 
            this.txtogrvelisoyad.AutoSize = true;
            this.txtogrvelisoyad.Location = new System.Drawing.Point(35, 48);
            this.txtogrvelisoyad.Name = "txtogrvelisoyad";
            this.txtogrvelisoyad.Size = new System.Drawing.Size(46, 13);
            this.txtogrvelisoyad.TabIndex = 15;
            this.txtogrvelisoyad.Text = "Soyad : ";
            // 
            // txtogvelinumara
            // 
            this.txtogvelinumara.AutoSize = true;
            this.txtogvelinumara.Location = new System.Drawing.Point(35, 79);
            this.txtogvelinumara.Name = "txtogvelinumara";
            this.txtogvelinumara.Size = new System.Drawing.Size(50, 13);
            this.txtogvelinumara.TabIndex = 16;
            this.txtogvelinumara.Text = "Numara :";
            // 
            // txtogvelimail
            // 
            this.txtogvelimail.AutoSize = true;
            this.txtogvelimail.Location = new System.Drawing.Point(35, 103);
            this.txtogvelimail.Name = "txtogvelimail";
            this.txtogvelimail.Size = new System.Drawing.Size(41, 13);
            this.txtogvelimail.TabIndex = 17;
            this.txtogvelimail.Text = "E-mail :";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.textBox1);
            this.groupBox2.Controls.Add(this.mtxtogrvelinumara);
            this.groupBox2.Controls.Add(this.txtogvelimail);
            this.groupBox2.Controls.Add(this.txtogvelinumara);
            this.groupBox2.Controls.Add(this.txtogrvelisoyad);
            this.groupBox2.Controls.Add(this.txtogveliad);
            this.groupBox2.Controls.Add(this.txtogvelisoyad);
            this.groupBox2.Controls.Add(this.txtogrveliad);
            this.groupBox2.Location = new System.Drawing.Point(521, 224);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(311, 162);
            this.groupBox2.TabIndex = 18;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Veli bilgileri";
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.Color.PaleTurquoise;
            this.textBox1.Location = new System.Drawing.Point(90, 102);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(132, 20);
            this.textBox1.TabIndex = 20;
            // 
            // mtxtogrvelinumara
            // 
            this.mtxtogrvelinumara.BackColor = System.Drawing.Color.PaleTurquoise;
            this.mtxtogrvelinumara.Location = new System.Drawing.Point(90, 72);
            this.mtxtogrvelinumara.Mask = "(999) 000-0000";
            this.mtxtogrvelinumara.Name = "mtxtogrvelinumara";
            this.mtxtogrvelinumara.Size = new System.Drawing.Size(132, 20);
            this.mtxtogrvelinumara.TabIndex = 19;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btnogrfoto);
            this.groupBox3.Controls.Add(this.pictureBox1);
            this.groupBox3.Controls.Add(this.cmbogrcinsiyet);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.txtogrno);
            this.groupBox3.Controls.Add(this.txttc);
            this.groupBox3.Controls.Add(this.txtogrsoyad);
            this.groupBox3.Controls.Add(this.txtograd);
            this.groupBox3.Location = new System.Drawing.Point(163, 224);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(328, 162);
            this.groupBox3.TabIndex = 19;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Ögrenci bilgileri";
            // 
            // btnogrfoto
            // 
            this.btnogrfoto.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.btnogrfoto.Location = new System.Drawing.Point(198, 100);
            this.btnogrfoto.Name = "btnogrfoto";
            this.btnogrfoto.Size = new System.Drawing.Size(110, 35);
            this.btnogrfoto.TabIndex = 15;
            this.btnogrfoto.Text = "fotoğraf ekle";
            this.btnogrfoto.UseVisualStyleBackColor = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(199, 19);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(109, 71);
            this.pictureBox1.TabIndex = 14;
            this.pictureBox1.TabStop = false;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.button4);
            this.groupBox4.Controls.Add(this.btnguncelle);
            this.groupBox4.Controls.Add(this.btnkaydet);
            this.groupBox4.Controls.Add(this.btnsil);
            this.groupBox4.Location = new System.Drawing.Point(163, 392);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(669, 71);
            this.groupBox4.TabIndex = 20;
            this.groupBox4.TabStop = false;
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.button4.Location = new System.Drawing.Point(477, 19);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(110, 35);
            this.button4.TabIndex = 3;
            this.button4.Text = "Toplu veri aktarma";
            this.button4.UseVisualStyleBackColor = false;
            // 
            // btnguncelle
            // 
            this.btnguncelle.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.btnguncelle.Location = new System.Drawing.Point(343, 19);
            this.btnguncelle.Name = "btnguncelle";
            this.btnguncelle.Size = new System.Drawing.Size(110, 35);
            this.btnguncelle.TabIndex = 2;
            this.btnguncelle.Text = "Güncelle";
            this.btnguncelle.UseVisualStyleBackColor = false;
            // 
            // btnkaydet
            // 
            this.btnkaydet.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.btnkaydet.Location = new System.Drawing.Point(53, 19);
            this.btnkaydet.Name = "btnkaydet";
            this.btnkaydet.Size = new System.Drawing.Size(110, 35);
            this.btnkaydet.TabIndex = 0;
            this.btnkaydet.Text = "Kaydet";
            this.btnkaydet.UseVisualStyleBackColor = false;
            // 
            // btnsil
            // 
            this.btnsil.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.btnsil.Location = new System.Drawing.Point(198, 19);
            this.btnsil.Name = "btnsil";
            this.btnsil.Size = new System.Drawing.Size(110, 35);
            this.btnsil.TabIndex = 1;
            this.btnsil.Text = "Sil";
            this.btnsil.UseVisualStyleBackColor = false;
            // 
            // Form3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Silver;
            this.ClientSize = new System.Drawing.Size(844, 462);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form3";
            this.Text = "3 Öğrenci İslemleri";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem öğrenciToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dİSİPLİNİŞLEMİToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem şİKAYETKUTUSUToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aYARLARToolStripMenuItem;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button btnsorgula;
        private System.Windows.Forms.ComboBox cmbsinif;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtograd;
        private System.Windows.Forms.TextBox txtogrsoyad;
        private System.Windows.Forms.TextBox txttc;
        private System.Windows.Forms.TextBox txtogrno;
        private System.Windows.Forms.TextBox txtogrveliad;
        private System.Windows.Forms.TextBox txtogvelisoyad;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cmbogrcinsiyet;
        private System.Windows.Forms.Label txtogveliad;
        private System.Windows.Forms.Label txtogrvelisoyad;
        private System.Windows.Forms.Label txtogvelinumara;
        private System.Windows.Forms.Label txtogvelimail;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.MaskedTextBox mtxtogrvelinumara;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnogrfoto;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button btnguncelle;
        private System.Windows.Forms.Button btnsil;
        private System.Windows.Forms.Button btnkaydet;
        private System.Windows.Forms.ToolStripMenuItem dİSİPLİNPROSEDÜRÜToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dİSİPİLİNSÖZLEŞMESİToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dİSİPLİNİŞLEMLERİToolStripMenuItem;
    }
}

