﻿namespace Disiplin_Proje
{
    partial class Form5
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.öĞRENCİİŞLEMLERİToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dİSToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dİSİPLİNİŞLEMLERİToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dİSİPLİNPROSEDÜRÜToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.şİKAYETKUTUSUToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aYARLARToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btngönder = new System.Windows.Forms.Button();
            this.btnword = new System.Windows.Forms.Button();
            this.richogretmen = new System.Windows.Forms.RichTextBox();
            this.richogrenci = new System.Windows.Forms.RichTextBox();
            this.menuStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Dock = System.Windows.Forms.DockStyle.Left;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.öĞRENCİİŞLEMLERİToolStripMenuItem,
            this.dİSToolStripMenuItem,
            this.şİKAYETKUTUSUToolStripMenuItem,
            this.aYARLARToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(132, 462);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // öĞRENCİİŞLEMLERİToolStripMenuItem
            // 
            this.öĞRENCİİŞLEMLERİToolStripMenuItem.Name = "öĞRENCİİŞLEMLERİToolStripMenuItem";
            this.öĞRENCİİŞLEMLERİToolStripMenuItem.Size = new System.Drawing.Size(119, 19);
            this.öĞRENCİİŞLEMLERİToolStripMenuItem.Text = "ÖĞRENCİ İŞLEMLERİ";
            this.öĞRENCİİŞLEMLERİToolStripMenuItem.Click += new System.EventHandler(this.öĞRENCİİŞLEMLERİToolStripMenuItem_Click);
            // 
            // dİSToolStripMenuItem
            // 
            this.dİSToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dİSİPLİNİŞLEMLERİToolStripMenuItem,
            this.dİSİPLİNPROSEDÜRÜToolStripMenuItem});
            this.dİSToolStripMenuItem.Name = "dİSToolStripMenuItem";
            this.dİSToolStripMenuItem.Size = new System.Drawing.Size(119, 19);
            this.dİSToolStripMenuItem.Text = "DİSİPLİN İŞLEMLERİ";
            // 
            // dİSİPLİNİŞLEMLERİToolStripMenuItem
            // 
            this.dİSİPLİNİŞLEMLERİToolStripMenuItem.Name = "dİSİPLİNİŞLEMLERİToolStripMenuItem";
            this.dİSİPLİNİŞLEMLERİToolStripMenuItem.Size = new System.Drawing.Size(188, 22);
            this.dİSİPLİNİŞLEMLERİToolStripMenuItem.Text = "DİSİPLİN İŞLEMLERİ";
            this.dİSİPLİNİŞLEMLERİToolStripMenuItem.Click += new System.EventHandler(this.dİSİPLİNİŞLEMLERİToolStripMenuItem_Click);
            // 
            // dİSİPLİNPROSEDÜRÜToolStripMenuItem
            // 
            this.dİSİPLİNPROSEDÜRÜToolStripMenuItem.Name = "dİSİPLİNPROSEDÜRÜToolStripMenuItem";
            this.dİSİPLİNPROSEDÜRÜToolStripMenuItem.Size = new System.Drawing.Size(188, 22);
            this.dİSİPLİNPROSEDÜRÜToolStripMenuItem.Text = "DİSİPLİN PROSEDÜRÜ";
            this.dİSİPLİNPROSEDÜRÜToolStripMenuItem.Click += new System.EventHandler(this.dİSİPLİNPROSEDÜRÜToolStripMenuItem_Click);
            // 
            // şİKAYETKUTUSUToolStripMenuItem
            // 
            this.şİKAYETKUTUSUToolStripMenuItem.Name = "şİKAYETKUTUSUToolStripMenuItem";
            this.şİKAYETKUTUSUToolStripMenuItem.Size = new System.Drawing.Size(119, 19);
            this.şİKAYETKUTUSUToolStripMenuItem.Text = "ŞİKAYET KUTUSU";
            // 
            // aYARLARToolStripMenuItem
            // 
            this.aYARLARToolStripMenuItem.Name = "aYARLARToolStripMenuItem";
            this.aYARLARToolStripMenuItem.Size = new System.Drawing.Size(119, 19);
            this.aYARLARToolStripMenuItem.Text = "AYARLAR";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.btngönder);
            this.groupBox1.Controls.Add(this.btnword);
            this.groupBox1.Controls.Add(this.richogretmen);
            this.groupBox1.Controls.Add(this.richogrenci);
            this.groupBox1.Location = new System.Drawing.Point(164, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(652, 414);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(41, 196);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(97, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Öğretmen dilekçesi";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(41, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Öğrenci ifadesi";
            // 
            // btngönder
            // 
            this.btngönder.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.btngönder.Location = new System.Drawing.Point(502, 350);
            this.btngönder.Name = "btngönder";
            this.btngönder.Size = new System.Drawing.Size(110, 35);
            this.btngönder.TabIndex = 3;
            this.btngönder.Text = "Gönder";
            this.btngönder.UseVisualStyleBackColor = false;
            this.btngönder.Click += new System.EventHandler(this.btngönder_Click);
            // 
            // btnword
            // 
            this.btnword.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.btnword.Location = new System.Drawing.Point(386, 350);
            this.btnword.Name = "btnword";
            this.btnword.Size = new System.Drawing.Size(110, 35);
            this.btnword.TabIndex = 2;
            this.btnword.Text = "Word sözleşme";
            this.btnword.UseVisualStyleBackColor = false;
            // 
            // richogretmen
            // 
            this.richogretmen.BackColor = System.Drawing.Color.PaleTurquoise;
            this.richogretmen.Location = new System.Drawing.Point(35, 212);
            this.richogretmen.Name = "richogretmen";
            this.richogretmen.Size = new System.Drawing.Size(577, 110);
            this.richogretmen.TabIndex = 1;
            this.richogretmen.Text = "";
            // 
            // richogrenci
            // 
            this.richogrenci.BackColor = System.Drawing.Color.PaleTurquoise;
            this.richogrenci.Location = new System.Drawing.Point(35, 52);
            this.richogrenci.Name = "richogrenci";
            this.richogrenci.Size = new System.Drawing.Size(577, 110);
            this.richogrenci.TabIndex = 0;
            this.richogrenci.Text = "";
            // 
            // Form5
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Silver;
            this.ClientSize = new System.Drawing.Size(844, 462);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form5";
            this.Text = " Sözlesme";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form5_FormClosing);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem öĞRENCİİŞLEMLERİToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dİSToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem şİKAYETKUTUSUToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aYARLARToolStripMenuItem;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btngönder;
        private System.Windows.Forms.Button btnword;
        private System.Windows.Forms.RichTextBox richogretmen;
        private System.Windows.Forms.RichTextBox richogrenci;
        private System.Windows.Forms.ToolStripMenuItem dİSİPLİNPROSEDÜRÜToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dİSİPLİNİŞLEMLERİToolStripMenuItem;
    }
}