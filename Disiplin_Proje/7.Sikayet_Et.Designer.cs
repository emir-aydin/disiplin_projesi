﻿namespace Disiplin_Proje
{
    partial class Sikayet_Et
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnsrgskyt = new System.Windows.Forms.Button();
            this.dataGogrskyt = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.btnnoskyt = new System.Windows.Forms.TextBox();
            this.richskyt = new System.Windows.Forms.RichTextBox();
            this.btnskyt = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGogrskyt)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnsrgskyt
            // 
            this.btnsrgskyt.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.btnsrgskyt.Location = new System.Drawing.Point(187, 19);
            this.btnsrgskyt.Name = "btnsrgskyt";
            this.btnsrgskyt.Size = new System.Drawing.Size(75, 23);
            this.btnsrgskyt.TabIndex = 1;
            this.btnsrgskyt.Text = "Sorgula";
            this.btnsrgskyt.UseVisualStyleBackColor = false;
            // 
            // dataGogrskyt
            // 
            this.dataGogrskyt.BackgroundColor = System.Drawing.Color.PaleTurquoise;
            this.dataGogrskyt.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGogrskyt.Location = new System.Drawing.Point(20, 59);
            this.dataGogrskyt.Name = "dataGogrskyt";
            this.dataGogrskyt.Size = new System.Drawing.Size(240, 150);
            this.dataGogrskyt.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(54, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(21, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "No";
            // 
            // btnnoskyt
            // 
            this.btnnoskyt.BackColor = System.Drawing.Color.PaleTurquoise;
            this.btnnoskyt.Location = new System.Drawing.Point(81, 22);
            this.btnnoskyt.Name = "btnnoskyt";
            this.btnnoskyt.Size = new System.Drawing.Size(100, 20);
            this.btnnoskyt.TabIndex = 4;
            // 
            // richskyt
            // 
            this.richskyt.BackColor = System.Drawing.Color.PaleTurquoise;
            this.richskyt.Location = new System.Drawing.Point(266, 59);
            this.richskyt.Name = "richskyt";
            this.richskyt.Size = new System.Drawing.Size(240, 150);
            this.richskyt.TabIndex = 5;
            this.richskyt.Text = "";
            // 
            // btnskyt
            // 
            this.btnskyt.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.btnskyt.Location = new System.Drawing.Point(396, 225);
            this.btnskyt.Name = "btnskyt";
            this.btnskyt.Size = new System.Drawing.Size(110, 35);
            this.btnskyt.TabIndex = 6;
            this.btnskyt.Text = "Şikayet Et";
            this.btnskyt.UseVisualStyleBackColor = false;
            this.btnskyt.Click += new System.EventHandler(this.button2_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Silver;
            this.groupBox1.Controls.Add(this.btnskyt);
            this.groupBox1.Controls.Add(this.btnnoskyt);
            this.groupBox1.Controls.Add(this.richskyt);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.dataGogrskyt);
            this.groupBox1.Controls.Add(this.btnsrgskyt);
            this.groupBox1.Location = new System.Drawing.Point(103, 16);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(536, 277);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Öğrenci Şikayet";
            // 
            // Sikayet_Et
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Silver;
            this.ClientSize = new System.Drawing.Size(844, 462);
            this.Controls.Add(this.groupBox1);
            this.Name = "Sikayet_Et";
            this.Text = "Sikayet_Et";
            this.Load += new System.EventHandler(this.Sikayet_Et_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGogrskyt)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnsrgskyt;
        private System.Windows.Forms.DataGridView dataGogrskyt;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox btnnoskyt;
        private System.Windows.Forms.RichTextBox richskyt;
        private System.Windows.Forms.Button btnskyt;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}