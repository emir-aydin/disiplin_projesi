﻿namespace Disiplin_Proje
{
    partial class Form1
    {
        /// <summary>
        ///Gerekli tasarımcı değişkeni.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///Kullanılan tüm kaynakları temizleyin.
        /// </summary>
        ///<param name="disposing">yönetilen kaynaklar dispose edilmeliyse doğru; aksi halde yanlış.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer üretilen kod

        /// <summary>
        /// Tasarımcı desteği için gerekli metot - bu metodun 
        ///içeriğini kod düzenleyici ile değiştirmeyin.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.öğrenciSorgulaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.şikayetEtToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbsinif = new System.Windows.Forms.ComboBox();
            this.btnyenile = new System.Windows.Forms.Button();
            this.btnsorgula = new System.Windows.Forms.Button();
            this.txtno = new System.Windows.Forms.TextBox();
            this.treeogrceza = new System.Windows.Forms.TreeView();
            this.dataGogr = new System.Windows.Forms.DataGridView();
            this.menuStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGogr)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Dock = System.Windows.Forms.DockStyle.Left;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.öğrenciSorgulaToolStripMenuItem,
            this.şikayetEtToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(140, 462);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // öğrenciSorgulaToolStripMenuItem
            // 
            this.öğrenciSorgulaToolStripMenuItem.Name = "öğrenciSorgulaToolStripMenuItem";
            this.öğrenciSorgulaToolStripMenuItem.Size = new System.Drawing.Size(127, 19);
            this.öğrenciSorgulaToolStripMenuItem.Text = "ÖğĞRENCİi SORGULA";
            // 
            // şikayetEtToolStripMenuItem
            // 
            this.şikayetEtToolStripMenuItem.Name = "şikayetEtToolStripMenuItem";
            this.şikayetEtToolStripMenuItem.Size = new System.Drawing.Size(127, 19);
            this.şikayetEtToolStripMenuItem.Text = "ŞİKAYET ET";
            this.şikayetEtToolStripMenuItem.Click += new System.EventHandler(this.şikayetEtToolStripMenuItem_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.cmbsinif);
            this.groupBox1.Controls.Add(this.btnyenile);
            this.groupBox1.Controls.Add(this.btnsorgula);
            this.groupBox1.Controls.Add(this.txtno);
            this.groupBox1.Controls.Add(this.treeogrceza);
            this.groupBox1.Controls.Add(this.dataGogr);
            this.groupBox1.Location = new System.Drawing.Point(153, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(679, 257);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Öğrenci Sorgula";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(18, 45);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(27, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Sınıf";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(21, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "No";
            // 
            // cmbsinif
            // 
            this.cmbsinif.BackColor = System.Drawing.Color.PaleTurquoise;
            this.cmbsinif.FormattingEnabled = true;
            this.cmbsinif.Location = new System.Drawing.Point(51, 45);
            this.cmbsinif.Name = "cmbsinif";
            this.cmbsinif.Size = new System.Drawing.Size(121, 21);
            this.cmbsinif.TabIndex = 6;
            // 
            // btnyenile
            // 
            this.btnyenile.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.btnyenile.Location = new System.Drawing.Point(270, 16);
            this.btnyenile.Name = "btnyenile";
            this.btnyenile.Size = new System.Drawing.Size(75, 23);
            this.btnyenile.TabIndex = 5;
            this.btnyenile.Text = "Yenile";
            this.btnyenile.UseVisualStyleBackColor = false;
            // 
            // btnsorgula
            // 
            this.btnsorgula.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.btnsorgula.Location = new System.Drawing.Point(180, 16);
            this.btnsorgula.Name = "btnsorgula";
            this.btnsorgula.Size = new System.Drawing.Size(75, 23);
            this.btnsorgula.TabIndex = 4;
            this.btnsorgula.Text = "Sorgula";
            this.btnsorgula.UseVisualStyleBackColor = false;
            this.btnsorgula.Click += new System.EventHandler(this.btnsorgula_Click);
            // 
            // txtno
            // 
            this.txtno.BackColor = System.Drawing.Color.PaleTurquoise;
            this.txtno.Location = new System.Drawing.Point(51, 19);
            this.txtno.Name = "txtno";
            this.txtno.Size = new System.Drawing.Size(121, 20);
            this.txtno.TabIndex = 3;
            // 
            // treeogrceza
            // 
            this.treeogrceza.BackColor = System.Drawing.Color.PaleTurquoise;
            this.treeogrceza.Location = new System.Drawing.Point(319, 82);
            this.treeogrceza.Name = "treeogrceza";
            this.treeogrceza.Size = new System.Drawing.Size(254, 150);
            this.treeogrceza.TabIndex = 2;
            // 
            // dataGogr
            // 
            this.dataGogr.BackgroundColor = System.Drawing.Color.PaleTurquoise;
            this.dataGogr.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGogr.Location = new System.Drawing.Point(21, 82);
            this.dataGogr.Name = "dataGogr";
            this.dataGogr.Size = new System.Drawing.Size(247, 150);
            this.dataGogr.TabIndex = 0;
            this.dataGogr.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGogr_CellContentClick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Silver;
            this.ClientSize = new System.Drawing.Size(844, 462);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "6.Öğretmen Giris";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGogr)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem öğrenciSorgulaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem şikayetEtToolStripMenuItem;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnsorgula;
        private System.Windows.Forms.TextBox txtno;
        private System.Windows.Forms.TreeView treeogrceza;
        private System.Windows.Forms.DataGridView dataGogr;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbsinif;
        private System.Windows.Forms.Button btnyenile;
    }
}

