﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Disiplin_Proje
{
    public partial class Form3 : Form
    {
        public Form3()
        {
            InitializeComponent();
        }

        private void ÖğrenciToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void dİSİPLİNİŞLEMİToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void dİSİPLİNPROSEDÜRÜToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void dİSİPLİNİŞLEMLERİToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form4 islemleri = new Form4();
            islemleri.ShowDialog();
        }

        private void dİSİPİLİNSÖZLEŞMESİToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form5 _5_Sözlesme = new Form5();
            _5_Sözlesme.ShowDialog();
        }
    }
}
