﻿namespace Disiplin_Proje
{
    partial class Form4
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.öĞRENCİİŞLEMLERİToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dİSİPLİNİŞLEMLERİToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dİSİPLİNİŞLEMLERİToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.dİSLİNSÖZLEŞMESİToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dİSİPLİNPROSEDÜRÜToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.şİKAYETKUTUSUToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aYARLARToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.btnyenile = new System.Windows.Forms.Button();
            this.btnsorgula = new System.Windows.Forms.Button();
            this.cmbogrsıifsorgula = new System.Windows.Forms.ComboBox();
            this.txtogrnosorgula = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnkaydet = new System.Windows.Forms.Button();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.txtdiger = new System.Windows.Forms.TextBox();
            this.cmbkabahat = new System.Windows.Forms.ComboBox();
            this.cmbkurul = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Dock = System.Windows.Forms.DockStyle.Left;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.öĞRENCİİŞLEMLERİToolStripMenuItem,
            this.dİSİPLİNİŞLEMLERİToolStripMenuItem,
            this.şİKAYETKUTUSUToolStripMenuItem,
            this.aYARLARToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(132, 509);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // öĞRENCİİŞLEMLERİToolStripMenuItem
            // 
            this.öĞRENCİİŞLEMLERİToolStripMenuItem.Name = "öĞRENCİİŞLEMLERİToolStripMenuItem";
            this.öĞRENCİİŞLEMLERİToolStripMenuItem.Size = new System.Drawing.Size(119, 19);
            this.öĞRENCİİŞLEMLERİToolStripMenuItem.Text = "ÖĞRENCİ İŞLEMLERİ";
            this.öĞRENCİİŞLEMLERİToolStripMenuItem.Click += new System.EventHandler(this.öĞRENCİİŞLEMLERİToolStripMenuItem_Click);
            // 
            // dİSİPLİNİŞLEMLERİToolStripMenuItem
            // 
            this.dİSİPLİNİŞLEMLERİToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dİSİPLİNİŞLEMLERİToolStripMenuItem1,
            this.dİSLİNSÖZLEŞMESİToolStripMenuItem,
            this.dİSİPLİNPROSEDÜRÜToolStripMenuItem});
            this.dİSİPLİNİŞLEMLERİToolStripMenuItem.Name = "dİSİPLİNİŞLEMLERİToolStripMenuItem";
            this.dİSİPLİNİŞLEMLERİToolStripMenuItem.Size = new System.Drawing.Size(119, 19);
            this.dİSİPLİNİŞLEMLERİToolStripMenuItem.Text = "DİSİPLİN İŞLEMLERİ";
            // 
            // dİSİPLİNİŞLEMLERİToolStripMenuItem1
            // 
            this.dİSİPLİNİŞLEMLERİToolStripMenuItem1.Name = "dİSİPLİNİŞLEMLERİToolStripMenuItem1";
            this.dİSİPLİNİŞLEMLERİToolStripMenuItem1.Size = new System.Drawing.Size(188, 22);
            this.dİSİPLİNİŞLEMLERİToolStripMenuItem1.Text = "DİSİPLİN İŞLEMLERİ";
            // 
            // dİSLİNSÖZLEŞMESİToolStripMenuItem
            // 
            this.dİSLİNSÖZLEŞMESİToolStripMenuItem.Name = "dİSLİNSÖZLEŞMESİToolStripMenuItem";
            this.dİSLİNSÖZLEŞMESİToolStripMenuItem.Size = new System.Drawing.Size(188, 22);
            this.dİSLİNSÖZLEŞMESİToolStripMenuItem.Text = "DİSİPLİN SÖZLEŞMESİ";
            this.dİSLİNSÖZLEŞMESİToolStripMenuItem.Click += new System.EventHandler(this.dİSLİNSÖZLEŞMESİToolStripMenuItem_Click);
            // 
            // dİSİPLİNPROSEDÜRÜToolStripMenuItem
            // 
            this.dİSİPLİNPROSEDÜRÜToolStripMenuItem.Name = "dİSİPLİNPROSEDÜRÜToolStripMenuItem";
            this.dİSİPLİNPROSEDÜRÜToolStripMenuItem.Size = new System.Drawing.Size(188, 22);
            this.dİSİPLİNPROSEDÜRÜToolStripMenuItem.Text = "DİSİPLİN PROSEDÜRÜ";
            this.dİSİPLİNPROSEDÜRÜToolStripMenuItem.Click += new System.EventHandler(this.dİSİPLİNPROSEDÜRÜToolStripMenuItem_Click);
            // 
            // şİKAYETKUTUSUToolStripMenuItem
            // 
            this.şİKAYETKUTUSUToolStripMenuItem.Name = "şİKAYETKUTUSUToolStripMenuItem";
            this.şİKAYETKUTUSUToolStripMenuItem.Size = new System.Drawing.Size(119, 19);
            this.şİKAYETKUTUSUToolStripMenuItem.Text = "ŞİKAYET KUTUSU";
            // 
            // aYARLARToolStripMenuItem
            // 
            this.aYARLARToolStripMenuItem.Name = "aYARLARToolStripMenuItem";
            this.aYARLARToolStripMenuItem.Size = new System.Drawing.Size(119, 19);
            this.aYARLARToolStripMenuItem.Text = "AYARLAR";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.treeView1);
            this.groupBox1.Controls.Add(this.dataGridView1);
            this.groupBox1.Controls.Add(this.btnyenile);
            this.groupBox1.Controls.Add(this.btnsorgula);
            this.groupBox1.Controls.Add(this.cmbogrsıifsorgula);
            this.groupBox1.Controls.Add(this.txtogrnosorgula);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(156, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(583, 263);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Öğrenci Sorgulama";
            // 
            // treeView1
            // 
            this.treeView1.BackColor = System.Drawing.Color.PaleTurquoise;
            this.treeView1.Location = new System.Drawing.Point(304, 85);
            this.treeView1.Name = "treeView1";
            this.treeView1.Size = new System.Drawing.Size(229, 150);
            this.treeView1.TabIndex = 7;
            // 
            // dataGridView1
            // 
            this.dataGridView1.BackgroundColor = System.Drawing.Color.PaleTurquoise;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(34, 85);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(229, 150);
            this.dataGridView1.TabIndex = 6;
            // 
            // btnyenile
            // 
            this.btnyenile.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.btnyenile.Location = new System.Drawing.Point(205, 47);
            this.btnyenile.Name = "btnyenile";
            this.btnyenile.Size = new System.Drawing.Size(69, 24);
            this.btnyenile.TabIndex = 5;
            this.btnyenile.Text = "Yenile";
            this.btnyenile.UseVisualStyleBackColor = false;
            // 
            // btnsorgula
            // 
            this.btnsorgula.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.btnsorgula.Location = new System.Drawing.Point(205, 24);
            this.btnsorgula.Name = "btnsorgula";
            this.btnsorgula.Size = new System.Drawing.Size(69, 24);
            this.btnsorgula.TabIndex = 4;
            this.btnsorgula.Text = "Sorugla";
            this.btnsorgula.UseVisualStyleBackColor = false;
            this.btnsorgula.Click += new System.EventHandler(this.btnsorgula_Click);
            // 
            // cmbogrsıifsorgula
            // 
            this.cmbogrsıifsorgula.BackColor = System.Drawing.Color.PaleTurquoise;
            this.cmbogrsıifsorgula.FormattingEnabled = true;
            this.cmbogrsıifsorgula.Location = new System.Drawing.Point(77, 50);
            this.cmbogrsıifsorgula.Name = "cmbogrsıifsorgula";
            this.cmbogrsıifsorgula.Size = new System.Drawing.Size(100, 21);
            this.cmbogrsıifsorgula.TabIndex = 3;
            // 
            // txtogrnosorgula
            // 
            this.txtogrnosorgula.BackColor = System.Drawing.Color.PaleTurquoise;
            this.txtogrnosorgula.Location = new System.Drawing.Point(77, 24);
            this.txtogrnosorgula.Name = "txtogrnosorgula";
            this.txtogrnosorgula.Size = new System.Drawing.Size(100, 20);
            this.txtogrnosorgula.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(38, 53);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Sınıf :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(38, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(27, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "No :";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnkaydet);
            this.groupBox2.Controls.Add(this.richTextBox1);
            this.groupBox2.Controls.Add(this.dateTimePicker1);
            this.groupBox2.Controls.Add(this.txtdiger);
            this.groupBox2.Controls.Add(this.cmbkabahat);
            this.groupBox2.Controls.Add(this.cmbkurul);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Location = new System.Drawing.Point(156, 281);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(583, 186);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Disiplin işlemleri";
            // 
            // btnkaydet
            // 
            this.btnkaydet.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.btnkaydet.Location = new System.Drawing.Point(439, 137);
            this.btnkaydet.Name = "btnkaydet";
            this.btnkaydet.Size = new System.Drawing.Size(110, 35);
            this.btnkaydet.TabIndex = 9;
            this.btnkaydet.Text = "Kaydet";
            this.btnkaydet.UseVisualStyleBackColor = false;
            // 
            // richTextBox1
            // 
            this.richTextBox1.BackColor = System.Drawing.Color.PaleTurquoise;
            this.richTextBox1.Location = new System.Drawing.Point(390, 35);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(159, 96);
            this.richTextBox1.TabIndex = 8;
            this.richTextBox1.Text = "";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(116, 53);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(153, 20);
            this.dateTimePicker1.TabIndex = 7;
            // 
            // txtdiger
            // 
            this.txtdiger.BackColor = System.Drawing.Color.PaleTurquoise;
            this.txtdiger.Location = new System.Drawing.Point(284, 110);
            this.txtdiger.Name = "txtdiger";
            this.txtdiger.Size = new System.Drawing.Size(100, 20);
            this.txtdiger.TabIndex = 6;
            this.txtdiger.Visible = false;
            // 
            // cmbkabahat
            // 
            this.cmbkabahat.BackColor = System.Drawing.Color.PaleTurquoise;
            this.cmbkabahat.FormattingEnabled = true;
            this.cmbkabahat.Location = new System.Drawing.Point(116, 110);
            this.cmbkabahat.Name = "cmbkabahat";
            this.cmbkabahat.Size = new System.Drawing.Size(153, 21);
            this.cmbkabahat.TabIndex = 4;
            // 
            // cmbkurul
            // 
            this.cmbkurul.BackColor = System.Drawing.Color.PaleTurquoise;
            this.cmbkurul.FormattingEnabled = true;
            this.cmbkurul.Location = new System.Drawing.Point(116, 80);
            this.cmbkurul.Name = "cmbkurul";
            this.cmbkurul.Size = new System.Drawing.Size(153, 21);
            this.cmbkurul.TabIndex = 3;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(22, 113);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(74, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "Kabahat türü :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(22, 83);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Kurul üyesi :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(22, 53);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(66, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Ceza Tarihi :";
            // 
            // Form4
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Silver;
            this.ClientSize = new System.Drawing.Size(800, 509);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form4";
            this.Text = "Disiplin islemleri";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form4_FormClosed);
            this.Load += new System.EventHandler(this.Form4_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem öĞRENCİİŞLEMLERİToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dİSİPLİNİŞLEMLERİToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dİSİPLİNPROSEDÜRÜToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dİSLİNSÖZLEŞMESİToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem şİKAYETKUTUSUToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aYARLARToolStripMenuItem;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox cmbogrsıifsorgula;
        private System.Windows.Forms.TextBox txtogrnosorgula;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button btnyenile;
        private System.Windows.Forms.Button btnsorgula;
        private System.Windows.Forms.TreeView treeView1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnkaydet;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.TextBox txtdiger;
        private System.Windows.Forms.ComboBox cmbkabahat;
        private System.Windows.Forms.ComboBox cmbkurul;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ToolStripMenuItem dİSİPLİNİŞLEMLERİToolStripMenuItem1;
    }
}